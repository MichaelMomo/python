import keyword

"""
    String allows us to store a seguance of characters
    that can be inclose inside single quotes or double quotes.
    String offers a baunch of methods

"""

brand ='SysaitCode'
print(brand)

# convert string value to uppercase
print(brand.upper())

# convert string value to lowercase
print(brand.lower())

# replace a character
print(brand.replace('a', '33'))

# retrieve a lenght
print(len(brand))

# compare String
print(brand != 'sysaitCode')

# checik if string content a word
print("Code" not in brand)

"""
    Multiline and formating Strings
"""

# this is not the best way to have multiline Strings but it walk
comment = "Hello everyone " \
            "my name is " \
            "Dongmo Michael " \
            "and i'm " \
            "56 years old."

# Best way
name = 'Michael Dongmo'
age = 56
comment1 = """
Hello everyone! 
My name is {}
and i'm {} years old.
"""
print(comment)

#format the String
print(comment1.format(name,age))

#another way to format is tu put f at the begining of string
userName = 'Mederic'
age = 30
email = f"""
Hello {userName}, 
you are {age} years old.
"""

print(email)

# Reserved Keywords
print(keyword.kwlist)