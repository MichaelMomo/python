# Variable is a placeholder where you can store a value

# name = "Michael"
# age = 24

# we can also do
name, age = "Michael", 24

pi = 3.14
number = [1, 2, 3, 4]

# When you wan to define value that can never change like a constant you use uppercase to define variable
AGE = 24

print(name)
print(age)
print(pi)
print(number)
print(AGE)