# in python you don't need to specify the type of varible

brand = "Sysaitcode"

# if you want to specify the datatype

brand: str = "Sysaitcode"
isAdult: bool = False

# in python, to define methode
    # this will be use in most of case because the datatype is only cheick at runtime and not at compile time
def hello() :
    return "Hello Michael"

# if you want to specify the return type, you do something like this
def hello() -> str :
     return "Hello"

 #  comments ----> this is a single line comment

"""
    This is a multiline comment

"""

print("hello")