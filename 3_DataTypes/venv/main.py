# when declaring a variable, you need to specify the type of value

name = "Michael"
age = 24
pi = 3.14
numbers = []
isAdult = True # the variable of type boolean can assume two value True or False

# let find the datatype of this variables

print(type(name))
print(type(age))
print(type(numbers))
print(type(pi))
print(type(isAdult))